# Kelompok 15
Anggota :
Nurul Nur Utami (4611420009)
Moh. Aminudin (4611420017)
M. Mustakim Surya (4611420041)

### Teknik Kompilasi
Teknik Kompilasi membuat bahasa pemrograman sendiri berbasis bahasa python.  
Spesifikasi : Python 

### Langkah untuk menjalankan:
python kompiler.py skrip.ind

### Syntax
"Tulis" untuk menulis dalam program  
"$a" untuk mendefinisikan variabel a  
lebih lengkapnya terdapat pada contoh dalam file skrip.ind